use std::fmt::{self, Display};

use crate::index::BitFieldIndex;

/// A size type.
///
/// `BitFieldSize` represents the size of an `IndexedBitField`.
///
/// Since `IndexedBitField`s can hold at most 128 items, as supported by a
/// `u128`, the value of `BitFieldSize` should never be more than 128. This
/// however is not enforced.
#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct BitFieldSize(u8);

impl BitFieldSize {
    /// Creates a `BitFieldSize` with the given value.
    ///
    /// Does not validate the argument.
    #[must_use]
    pub const fn new(value: u8) -> Self { Self(value) }

    /// Consumes the `BitFieldSize`, returning the wrapped value.
    #[must_use]
    pub const fn into_inner(self) -> u8 { self.0 }
}

impl From<u8> for BitFieldSize {
    fn from(value: u8) -> Self { Self(value) }
}

impl PartialEq<BitFieldIndex> for BitFieldSize {
    fn eq(&self, other: &BitFieldIndex) -> bool { self.0.eq(&other.into_inner()) }
}

impl PartialEq<u8> for BitFieldSize {
    fn eq(&self, other: &u8) -> bool { self.0.eq(other) }
}

impl PartialOrd<BitFieldIndex> for BitFieldSize {
    fn partial_cmp(&self, other: &BitFieldIndex) -> Option<std::cmp::Ordering> {
        self.0.partial_cmp(&other.into_inner())
    }
}

impl Display for BitFieldSize {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result { write!(f, "{}", self.0) }
}
