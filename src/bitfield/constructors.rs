use std::cmp::Ordering;

use super::*;

impl<T: PrimInt> BitField<T> {
    #[inline]
    #[must_use]
    pub fn fill(nb_bits: BitFieldSize) -> Self {
        if nb_bits >= Self::SIZE {
            Self::full()
        } else {
            Self((<T>::one() << (nb_bits.into_inner() as usize)) - <T>::one())
        }
    }

    #[inline]
    #[must_use]
    pub fn fill_checked(nb_bits: BitFieldSize) -> Option<Self> {
        match Self::SIZE.cmp(&nb_bits) {
            Ordering::Less => None,
            Ordering::Equal => Some(Self::full()),
            Ordering::Greater => {
                Some(Self((<T>::one() << (nb_bits.into_inner() as usize)) - <T>::one()))
            },
        }
    }

    #[inline]
    #[must_use]
    pub fn full() -> Self { Self(<T as num_traits::Bounded>::max_value()) }

    #[inline]
    #[must_use]
    pub fn empty() -> Self { Self(T::zero()) }

    #[inline]
    #[must_use]
    pub fn single_bit(bit_idx: BitFieldIndex) -> Self { Self(T::one()) << bit_idx.into_inner() }

    #[inline]
    #[must_use]
    pub fn single_bit_checked(bit_idx: BitFieldIndex) -> Option<Self> {
        if bit_idx >= Self::SIZE {
            None
        } else {
            Some(Self::single_bit(bit_idx))
        }
    }

    #[inline]
    #[must_use]
    pub fn all_but_single_bit(nb_bits: BitFieldSize, bit_idx: BitFieldIndex) -> Self {
        (Self::full() ^ Self::single_bit(bit_idx)) & Self::fill(nb_bits)
    }

    #[inline]
    #[must_use]
    pub fn all_but_single_bit_checked(
        nb_bits: BitFieldSize,
        bit_idx: BitFieldIndex,
    ) -> Option<Self> {
        match (Self::fill_checked(nb_bits), Self::single_bit_checked(bit_idx)) {
            (Some(bit_idx), Some(single_bit)) => Some((Self::full() ^ single_bit) & bit_idx),
            _ => None,
        }
    }

    #[inline]
    #[must_use]
    pub fn all_but_single_bit_full(bit_idx: BitFieldIndex) -> Self {
        Self::full() ^ Self::single_bit(bit_idx)
    }

    #[inline]
    #[must_use]
    pub fn all_but_single_bit_full_checked(bit_idx: BitFieldIndex) -> Option<Self> {
        Self::single_bit_checked(bit_idx).map(|single_bit| Self::full() ^ single_bit)
    }
}

impl<T: PrimInt> From<T> for BitField<T> {
    fn from(value: T) -> Self { Self(value) }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn fill() {
        assert_eq!(
            Some(0b0000_0111u8),
            BitField::<u8>::fill_checked(3.into()).map(BitField::into_inner)
        );
        assert_eq!(
            Some(0b11_1111_1111_1111u16),
            BitField::<u16>::fill_checked(14.into()).map(BitField::into_inner)
        );
        assert_eq!(
            Some(0b1111_1111_1111_1111u16),
            BitField::<u16>::fill_checked(16.into()).map(BitField::into_inner)
        );

        assert_eq!(None, BitField::<u8>::fill_checked(16.into()));
    }

    #[test]
    fn empty_full() {
        assert_eq!(0, BitField::<u128>::empty().into_inner());
        assert_eq!(0b1111_1111_1111_1111u16, BitField::<u16>::full().into_inner());
    }

    #[test]
    fn single_bit() {
        assert_eq!(0b10, BitField::<u8>::single_bit(1.into()).into_inner());
        assert_eq!(0b1000_0000, BitField::<u8>::single_bit(7.into()).into_inner());

        assert_eq!(None, BitField::<u8>::single_bit_checked(8.into()).map(BitField::into_inner));
    }

    #[test]
    fn all_but_single_bit() {
        assert_eq!(0b1011, BitField::<u8>::all_but_single_bit(4.into(), 2.into()).into_inner());
        assert_eq!(
            BitField::<u8>::fill_checked(5.into()),
            Some(BitField::<u8>::all_but_single_bit(5.into(), 5.into()))
        );

        assert_eq!(0b1111_1011, BitField::<u8>::all_but_single_bit_full(2.into()).into_inner());

        assert_eq!(None, BitField::<u8>::all_but_single_bit_full_checked(8.into()));

        assert_eq!(
            Some(0b11011),
            BitField::<u8>::all_but_single_bit_checked(5.into(), 2.into())
                .map(BitField::into_inner)
        );
        assert_eq!(None, BitField::<u8>::all_but_single_bit_checked(16.into(), 2.into()));
        assert_eq!(None, BitField::<u8>::all_but_single_bit_checked(5.into(), 8.into()));
    }
}
