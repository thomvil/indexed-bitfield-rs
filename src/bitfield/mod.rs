use num_traits::PrimInt;

use crate::{BitFieldIndex, BitFieldSize};

mod accessors;
mod constructors;
mod inspectors;
mod ops;

/// A bitfield type.
///
/// Built on type of an unsigned integer. It uses the bits to represent the
/// presence or absence of elements.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct BitField<T: PrimInt>(T);

impl<T: PrimInt> BitField<T> {
    /// The maximum number of elements that can be tracked.
    #[allow(clippy::cast_possible_truncation)]
    pub const SIZE: BitFieldSize = BitFieldSize::new(std::mem::size_of::<T>() as u8 * 8);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn size() {
        assert_eq!(BitFieldSize::from(8), BitField::<u8>::SIZE);
        assert_eq!(BitFieldSize::from(16), BitField::<u16>::SIZE);
        assert_eq!(BitFieldSize::from(32), BitField::<u32>::SIZE);
        assert_eq!(BitFieldSize::from(64), BitField::<u64>::SIZE);
        assert_eq!(BitFieldSize::from(128), BitField::<u128>::SIZE);
    }
}
