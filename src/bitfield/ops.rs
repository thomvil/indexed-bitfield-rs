use super::{BitField, PrimInt};

impl<T: PrimInt> std::ops::BitAnd for BitField<T> {
    type Output = Self;

    fn bitand(self, rhs: Self) -> Self::Output { Self(*self & *rhs) }
}

impl<T: PrimInt> std::ops::BitAndAssign for BitField<T> {
    fn bitand_assign(&mut self, rhs: Self) { *self = *self & rhs; }
}

impl<T: PrimInt> std::ops::BitOr for BitField<T> {
    type Output = Self;

    fn bitor(self, rhs: Self) -> Self::Output { Self(*self | *rhs) }
}

impl<T: PrimInt> std::ops::BitOrAssign for BitField<T> {
    fn bitor_assign(&mut self, rhs: Self) { *self = *self | rhs; }
}

impl<T: PrimInt> std::ops::BitXor for BitField<T> {
    type Output = Self;

    fn bitxor(self, rhs: Self) -> Self::Output { Self(*self ^ *rhs) }
}

impl<T: PrimInt> std::ops::BitXorAssign for BitField<T> {
    fn bitxor_assign(&mut self, rhs: Self) { *self = *self ^ rhs; }
}

impl<T: PrimInt> std::ops::Not for BitField<T> {
    type Output = Self;

    fn not(self) -> Self::Output { Self(!*self) }
}

impl<T: PrimInt> std::ops::Shl<u8> for BitField<T> {
    type Output = Self;

    fn shl(self, rhs: u8) -> Self::Output { Self(*self << (rhs as usize)) }
}

impl<T: PrimInt> std::ops::ShlAssign<u8> for BitField<T> {
    fn shl_assign(&mut self, rhs: u8) { *self = *self << rhs; }
}

impl<T: PrimInt> std::ops::Shr<u8> for BitField<T> {
    type Output = Self;

    fn shr(self, rhs: u8) -> Self::Output { Self(*self >> (rhs as usize)) }
}

impl<T: PrimInt> std::ops::ShrAssign<u8> for BitField<T> {
    fn shr_assign(&mut self, rhs: u8) { *self = *self >> rhs; }
}

impl<T: PrimInt> std::ops::Deref for BitField<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target { &self.0 }
}

impl<T: PrimInt> std::ops::DerefMut for BitField<T> {
    fn deref_mut(&mut self) -> &mut Self::Target { &mut self.0 }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn ops() {
        let bf2 = BitField::<u8>::fill(3.into());
        let bf3 = BitField::<u8>::fill(3.into());
        assert_eq!(bf2, bf2 & bf3);
        assert_eq!(bf3, bf2 | bf3);
    }
}
