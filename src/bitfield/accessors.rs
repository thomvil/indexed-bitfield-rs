use super::*;

impl<T: PrimInt> BitField<T> {
    /// Set the bit at `BitFieldIndex` to 1.
    ///
    /// Does not check that the `BitFieldIndex` is not out of range.
    ///
    /// Returns `true` if the value is changed.
    #[inline]
    pub fn set_bit(&mut self, bit_idx: BitFieldIndex) -> bool {
        let new_value = *self | Self::single_bit(bit_idx);
        let res = new_value != *self;
        *self = new_value;
        res
    }

    /// Set the bit at `BitFieldIndex` to 1, if the index is in range..
    ///
    /// Check that the `BitFieldIndex` is not out of range. Calls `set_bit` if
    /// the index is in range.
    ///
    /// Returns `Some(true)` if the value is changed. Returns `None` if the
    /// index is out of range.
    #[inline]
    pub fn set_bit_checked(&mut self, bit_idx: BitFieldIndex) -> Option<bool> {
        Some(()).filter(|_| bit_idx < Self::SIZE)?;
        Some(self.set_bit(bit_idx))
    }

    #[inline]
    pub fn unset_bit(&mut self, bit_idx: BitFieldIndex) -> bool {
        let new_value = *self & Self::all_but_single_bit_full(bit_idx);
        let res = new_value != *self;
        *self = new_value;
        res
    }

    #[inline]
    pub fn unset_bit_checked(&mut self, bit_idx: BitFieldIndex) -> Option<bool> {
        Some(()).filter(|_| bit_idx < Self::SIZE)?;
        Some(self.set_bit(bit_idx))
    }

    #[inline]
    pub fn merge(&mut self, other: &Self) -> bool {
        let new_value = *self | *other;
        let res = new_value != *self;
        *self = new_value;
        res
    }
}
