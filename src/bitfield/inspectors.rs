use super::*;

impl<T: PrimInt> BitField<T> {
    /// Consumes the `BitField`, returning the wrapped value.
    #[inline]
    pub const fn into_inner(self) -> T { self.0 }

    /// Returns the number of elements present.
    #[inline]
    pub fn nb_elements(&self) -> usize { self.0.count_ones() as usize }
}
