use num_traits::PrimInt;

use crate::{bitfield::BitField, BitFieldIndex, BitFieldSize};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct IndexedBitField<T: PrimInt> {
    size:  BitFieldSize,
    field: BitField<T>,
}

impl<T: PrimInt> IndexedBitField<T> {
    pub const MAX_SIZE: BitFieldSize = BitField::<T>::SIZE;

    #[inline]
    fn new(size: BitFieldSize, field: BitField<T>) -> Self { Self { size, field } }

    #[inline]
    #[must_use]
    pub fn full() -> Self { Self::full_of(Self::MAX_SIZE) }

    #[inline]
    #[must_use]
    pub fn empty() -> Self { Self::empty_of(Self::MAX_SIZE) }

    #[inline]
    #[must_use]
    pub fn full_of(size: BitFieldSize) -> Self { Self::new(size, BitField::<T>::fill(size)) }

    #[inline]
    #[must_use]
    pub fn empty_of(size: BitFieldSize) -> Self { Self::new(size, BitField::<T>::empty()) }

    #[inline]
    pub fn into_inner(self) -> (BitFieldSize, BitField<T>) { (self.size, self.field) }

    #[inline]
    pub fn bits(self) -> BitField<T> { self.field }

    #[inline]
    pub fn size(self) -> BitFieldSize { self.size }

    #[inline]
    pub fn is_empty(&self) -> bool { *self == Self::empty_of(self.size) }

    #[inline]
    pub fn clear(&mut self) { *self = Self::empty_of(self.size) }

    #[inline]
    pub fn restore_full(&mut self) { *self = Self::full_of(self.size); }

    #[inline]
    pub fn resize(&mut self, new_size: BitFieldSize) { self.size = new_size; }

    #[inline]
    pub fn resize_checked(&mut self, new_size: BitFieldSize) -> bool {
        if new_size > Self::MAX_SIZE {
            false
        } else {
            self.size = new_size;
            true
        }
    }

    #[inline]
    pub fn nb_elements(&self) -> usize { self.field.nb_elements() }

    #[inline]
    pub fn set_bit(&mut self, bit_idx: BitFieldIndex) -> bool { self.field.set_bit(bit_idx) }

    #[inline]
    pub fn unset_bit(&mut self, bit_idx: BitFieldIndex) -> bool { self.field.unset_bit(bit_idx) }

    #[inline]
    pub fn merge(&mut self, other: &Self) -> bool {
        self.size = std::cmp::max(self.size, other.size);
        self.field.merge(&other.field)
    }

    #[inline]
    #[allow(clippy::cast_possible_truncation)]
    pub fn largest(&self) -> Option<BitFieldIndex> {
        if self.is_empty() {
            None
        } else {
            let idx = Self::MAX_SIZE.into_inner() - (self.field.leading_zeros() as u8) - 1;
            Some(BitFieldIndex::from(idx))
        }
    }

    #[inline]
    pub fn pop_largest(&mut self) -> Option<BitFieldIndex> {
        self.largest().map(|idx| {
            self.unset_bit(idx);
            idx
        })
    }

    #[inline]
    #[allow(clippy::cast_possible_truncation)]
    pub fn smallest(&self) -> Option<BitFieldIndex> {
        if self.is_empty() {
            None
        } else {
            let idx = self.field.trailing_zeros() as u8;
            Some(BitFieldIndex::from(idx))
        }
    }

    #[inline]
    pub fn pop_smallest(&mut self) -> Option<BitFieldIndex> {
        self.smallest().map(|idx| {
            self.unset_bit(idx);
            idx
        })
    }

    #[inline]
    pub fn nth_smallest(&self, n: usize) -> Option<BitFieldIndex> {
        let nb_elements = self.nb_elements();
        match n {
            0 => self.smallest(),
            i if i >= nb_elements => None,
            i if i == nb_elements - 1 => self.largest(),
            i if i > (nb_elements / 2) => self.nth_largest(nb_elements - n - 1),
            i => {
                let mut res = *self;
                (0..i).for_each(|_| {
                    res.pop_smallest();
                });
                res.smallest()
            },
        }
    }

    #[inline]
    pub fn pop_nth_smallest(&mut self, n: usize) -> Option<BitFieldIndex> {
        self.nth_smallest(n).map(|idx| {
            self.unset_bit(idx);
            idx
        })
    }

    #[inline]
    pub fn nth_largest(&self, n: usize) -> Option<BitFieldIndex> {
        let nb_elements = self.nb_elements();
        match n {
            0 => self.largest(),
            i if i >= nb_elements => None,
            i if i == nb_elements - 1 => self.smallest(),
            i if i > (nb_elements / 2) => self.nth_smallest(nb_elements - n - 1),
            i => {
                let mut res = *self;
                (0..i).for_each(|_| {
                    res.pop_largest();
                });
                res.largest()
            },
        }
    }

    #[inline]
    pub fn pop_nth_largest(&mut self, n: usize) -> Option<BitFieldIndex> {
        self.nth_largest(n).map(|idx| {
            self.unset_bit(idx);
            idx
        })
    }

    #[inline]
    pub fn poll(&self, bit_idx: BitFieldIndex) -> bool {
        (self.field & BitField::<T>::single_bit(bit_idx)) != BitField::<T>::empty()
    }

    #[inline]
    fn new_checked(size: BitFieldSize, field_opt: Option<BitField<T>>) -> Option<Self> {
        field_opt.map(|field| Self { size, field })
    }

    #[inline]
    #[must_use]
    pub fn full_of_checked(size: BitFieldSize) -> Option<Self> {
        Self::new_checked(size, BitField::<T>::fill_checked(size))
    }

    #[inline]
    #[must_use]
    pub fn empty_of_checked(size: BitFieldSize) -> Option<Self> {
        let _ = Self::full_of_checked(size)?;
        Self::new_checked(size, Some(BitField::<T>::empty()))
    }

    #[inline]
    pub fn set_bit_checked(&mut self, bit_idx: BitFieldIndex) -> Option<bool> {
        if bit_idx >= self.size {
            return None;
        }
        Some(self.set_bit(bit_idx))
    }

    #[inline]
    pub fn unset_bit_checked(&mut self, bit_idx: BitFieldIndex) -> Option<bool> {
        if bit_idx >= self.size {
            return None;
        }
        Some(self.set_bit(bit_idx))
    }
}

#[cfg(test)]
mod tests {
    use num_traits::PrimInt;

    use crate::{
        BitField as BF,
        BitFieldIndex as BFI,
        BitFieldSize as BFS,
        IndexedBitField as IBF,
    };

    fn field<T: PrimInt>(size: u8) -> Option<BF<T>> {
        IBF::<T>::full_of_checked(size.into()).map(|ibf| ibf.into_inner().1)
    }

    #[test]
    fn into_inner() {
        let (size, field) = IBF::<u8>::full_of(3.into()).into_inner();
        assert_eq!(BFS::from(3), size);
        assert_eq!(BF::<u8>::from(0b111), field);
    }

    #[test]
    fn constructors() {
        assert_eq!((BFS::from(8), BF::<u8>::from(0b1111_1111)), IBF::<u8>::full().into_inner());
        assert_eq!((BFS::from(8), BF::<u8>::from(0b0000_0000)), IBF::<u8>::empty().into_inner());
        assert_eq!((BFS::from(128), BF::<u128>::from(u128::MAX)), IBF::<u128>::full().into_inner());

        assert_eq!(
            (BFS::from(5), BF::<u8>::from(0b11111)),
            IBF::<u8>::full_of(5.into()).into_inner()
        );
        assert_eq!(
            (BFS::from(5), BF::<u8>::from(0b00000)),
            IBF::<u8>::empty_of(5.into()).into_inner()
        );

        assert!(IBF::<u8>::full_of_checked(5.into()).is_some());
        assert!(IBF::<u8>::empty_of_checked(8.into()).is_some());
        assert!(IBF::<u8>::full_of_checked(9.into()).is_none());

        assert_eq!(Some(BF::<u8>::from(0b11111)), field::<u8>(5));
        assert_eq!(None, field::<u8>(9));
        assert_eq!(Some(BF::<u16>::from(0b1_1111_1111)), field::<u16>(9));
    }

    #[test]
    fn bits() {
        assert_eq!(BF::<u8>::from(0b1111_1111), IBF::<u8>::full().bits());
        assert_eq!(BF::<u8>::from(0b0000_0000), IBF::<u8>::empty().bits());
        assert_eq!(BF::<u128>::from(u128::MAX), IBF::<u128>::full().bits());

        assert_eq!(BF::<u8>::from(0b11111), IBF::<u8>::full_of(5.into()).bits());
        assert_eq!(BF::<u8>::from(0b00000), IBF::<u8>::empty_of(5.into()).bits());
    }

    #[test]
    fn len() {
        assert_eq!(BFS::from(8), IBF::<u8>::full().size());
        assert_eq!(BFS::from(8), IBF::<u8>::empty().size());
        assert_eq!(BFS::from(128), IBF::<u128>::full().size());

        assert_eq!(BFS::from(5), IBF::<u8>::full_of(5.into()).size());
        assert_eq!(BFS::from(5), IBF::<u8>::empty_of(5.into()).size());
    }

    #[test]
    fn is_empty() {
        assert!(!IBF::<u8>::full().is_empty());
        assert!(IBF::<u8>::empty().is_empty());
    }

    #[test]
    fn clear() {
        let mut ibf = IBF::<u8>::full();
        assert!(!ibf.is_empty());
        ibf.clear();
        assert!(ibf.is_empty());
    }

    #[test]
    fn restore_full() {
        let mut ibf = IBF::<u8>::empty_of(5.into());
        assert!(ibf.is_empty());
        ibf.restore_full();
        assert!(!ibf.is_empty());
        assert_eq!(5, ibf.nb_elements());
    }

    #[test]
    fn set_bit() {
        let mut ibf = IBF::<u8>::empty_of(5.into());
        assert_eq!(BF::<u8>::from(0b00000), ibf.bits());
        let flag = ibf.set_bit(0.into());
        assert!(flag);
        assert_eq!(BF::<u8>::from(0b00001), ibf.bits());
        let flag = ibf.set_bit(4.into());
        assert!(flag);
        assert_eq!(BF::<u8>::from(0b10001), ibf.bits());
        let flag = ibf.set_bit(4.into());
        assert!(!flag);
    }

    #[test]
    fn unset_bit() {
        let mut ibf = IBF::<u8>::full_of(5.into());
        assert_eq!(BF::<u8>::from(0b11111), ibf.bits());
        let flag = ibf.unset_bit(0.into());
        assert!(flag);
        assert_eq!(BF::<u8>::from(0b11110), ibf.bits());
        let flag = ibf.unset_bit(4.into());
        assert!(flag);
        assert_eq!(BF::<u8>::from(0b01110), ibf.bits());
        let flag = ibf.unset_bit(4.into());
        assert!(!flag);
    }

    #[test]
    fn nb_elements() {
        let mut ibf = IBF::<u8>::empty_of(5.into());
        assert_eq!(0, ibf.nb_elements());
        let _ = ibf.set_bit(0.into());
        assert_eq!(1, ibf.nb_elements());
        let _ = ibf.set_bit(4.into());
        assert_eq!(2, ibf.nb_elements());
        let _ = ibf.set_bit(4.into());
        assert_eq!(2, ibf.nb_elements());
    }

    #[test]
    fn merge() {
        let mut ibf1 = IBF::<u8>::empty_of(4.into());
        ibf1.set_bit(3.into());
        assert_eq!(BFS::from(4), ibf1.size());
        assert_eq!(BF::<u8>::from(0b1000), ibf1.bits());

        let mut ibf2 = IBF::<u8>::empty_of(5.into());
        ibf2.set_bit(1.into());
        ibf2.set_bit(2.into());
        assert_eq!(BFS::from(5), ibf2.size());
        assert_eq!(BF::<u8>::from(0b00110), ibf2.bits());

        let flag = ibf1.merge(&ibf2);
        assert!(flag);
        assert_eq!(BFS::from(5), ibf1.size());
        assert_eq!(BF::<u8>::from(0b01110), ibf1.bits());
    }

    #[test]
    fn largest() {
        let mut ibf = IBF::<u8>::full_of(4.into());
        assert_eq!(Some(BFI::from(3)), ibf.largest());
        assert_eq!(Some(BFI::from(3)), ibf.pop_largest());
        assert_eq!(Some(BFI::from(2)), ibf.largest());
    }

    #[test]
    fn smallest() {
        let mut ibf = IBF::<u8>::full_of(4.into());
        assert_eq!(Some(BFI::from(0)), ibf.smallest());
        assert_eq!(Some(BFI::from(0)), ibf.pop_smallest());
        assert_eq!(Some(BFI::from(1)), ibf.smallest());
    }

    #[test]
    fn poll() {
        let mut ibf = IBF::<u8>::empty_of(4.into());
        assert!(!ibf.poll(0.into()));
        assert!(!ibf.poll(1.into()));
        assert!(!ibf.poll(2.into()));
        assert!(!ibf.poll(3.into()));

        ibf.set_bit(1.into());
        assert!(!ibf.poll(0.into()));
        assert!(ibf.poll(1.into()));
        assert!(!ibf.poll(2.into()));
        assert!(!ibf.poll(3.into()));
    }

    #[test]
    fn nth() {
        let ibf = IBF::<u8>::full();
        assert_eq!(Some(BFI::from(7)), ibf.nth_largest(0));
        assert_eq!(Some(BFI::from(6)), ibf.nth_largest(1));
        assert_eq!(Some(BFI::from(5)), ibf.nth_largest(2));
        assert_eq!(Some(BFI::from(4)), ibf.nth_largest(3));
        assert_eq!(Some(BFI::from(3)), ibf.nth_largest(4));
        assert_eq!(Some(BFI::from(2)), ibf.nth_largest(5));
        assert_eq!(Some(BFI::from(1)), ibf.nth_largest(6));
        assert_eq!(Some(BFI::from(0)), ibf.nth_largest(7));

        let ibf = IBF::<u8>::full();
        assert_eq!(Some(BFI::from(0)), ibf.nth_smallest(0));
        assert_eq!(Some(BFI::from(1)), ibf.nth_smallest(1));
        assert_eq!(Some(BFI::from(2)), ibf.nth_smallest(2));
        assert_eq!(Some(BFI::from(3)), ibf.nth_smallest(3));
        assert_eq!(Some(BFI::from(4)), ibf.nth_smallest(4));
        assert_eq!(Some(BFI::from(5)), ibf.nth_smallest(5));
        assert_eq!(Some(BFI::from(6)), ibf.nth_smallest(6));
        assert_eq!(Some(BFI::from(7)), ibf.nth_smallest(7));

        let mut ibf = IBF::<u8>::full_of(4.into());
        assert_eq!(4, ibf.nb_elements());
        assert_eq!(Some(BFI::from(0)), ibf.nth_largest(3));
        assert_eq!(Some(BFI::from(0)), ibf.pop_nth_largest(3));
        assert_eq!(3, ibf.nb_elements());
        assert_eq!(Some(BFI::from(2)), ibf.nth_largest(1));
        assert_eq!(Some(BFI::from(2)), ibf.pop_nth_largest(1));
        assert_eq!(2, ibf.nb_elements());

        let mut ibf = IBF::<u8>::full_of(4.into());
        assert_eq!(4, ibf.nb_elements());
        assert_eq!(Some(BFI::from(2)), ibf.nth_smallest(2));
        assert_eq!(Some(BFI::from(2)), ibf.pop_nth_smallest(2));
        assert_eq!(3, ibf.nb_elements());
    }

    #[test]
    fn resize() {
        let mut ibf = IBF::<u8>::full_of(4.into());
        assert_eq!(4, ibf.nb_elements());
        assert_eq!(BFS::from(4), ibf.size());
        ibf.resize(5.into());
        assert_eq!(4, ibf.nb_elements());
        assert_eq!(BFS::from(5), ibf.size());
        ibf.restore_full();
        assert_eq!(5, ibf.nb_elements());

        let old_size = ibf.size();
        let flag = ibf.resize_checked(9.into());
        assert!(!flag);
        assert_eq!(old_size, ibf.size());
    }
}
