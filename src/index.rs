use crate::size::BitFieldSize;

/// An index type.
///
/// `BitFieldIndex` is used to index an `IndexedBitField`.
///
/// Represents the corresponding bit in a little-endian representation.
/// `BitFieldIndex` is zero-indexed.
#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct BitFieldIndex(u8);

impl BitFieldIndex {
    /// Creates a `BitFieldIndex` with the given value.
    ///
    /// Does not validate the argument.
    #[must_use]
    pub const fn new(value: u8) -> Self { Self(value) }

    /// Consumes the `BitFieldSize`, returning the wrapped value.
    #[must_use]
    pub fn into_inner(self) -> u8 { self.0 }
}

impl From<u8> for BitFieldIndex {
    fn from(value: u8) -> Self { Self(value) }
}

impl PartialEq<BitFieldSize> for BitFieldIndex {
    fn eq(&self, other: &BitFieldSize) -> bool { self.0.eq(&other.into_inner()) }
}

impl PartialOrd<BitFieldSize> for BitFieldIndex {
    fn partial_cmp(&self, other: &BitFieldSize) -> Option<std::cmp::Ordering> {
        self.0.partial_cmp(&other.into_inner())
    }
}
