//! This crate is a simple bitfield that can be used to track presence or
//! absence of elements in a known collection of a specified size.
//!
//! Under the hood it uses unsigned integers. The bits represent presence of
//! absence of elements. No reference to a collection is made explicitly.

// #![warn(clippy::pedantic, missing_docs)]
#![allow(clippy::module_name_repetitions)]

mod bitfield;
mod index;
mod indexed_bitfield;
mod size;

pub use bitfield::BitField;
pub use index::BitFieldIndex;
pub use indexed_bitfield::IndexedBitField;
pub use size::BitFieldSize;

/// Type alias for an `IndexedBitField` with at most 8 items.
pub type IndexedBitField8 = IndexedBitField<u8>;

/// Type alias for an `IndexedBitField` with at most 16 items.
pub type IndexedBitField16 = IndexedBitField<u16>;

/// Type alias for an `IndexedBitField` with at most 32 items.
pub type IndexedBitField32 = IndexedBitField<u32>;

/// Type alias for an `IndexedBitField` with at most 64 items.
pub type IndexedBitField64 = IndexedBitField<u64>;

/// Type alias for an `IndexedBitField` with at most 128 items.
pub type IndexedBitField128 = IndexedBitField<u128>;
